<?php    
	error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);

	class expressScript{
		
		public $tgtbwip;
		public $tgtbwprotocol;
		public $tgtbwuserid;
		public $tgtbwpassword;
		public $tgtbwport;
		public $ociVersion;
		public $targetclient;
		public $tgtsessionid;
		public $securityDomainPattern;
		public $csvDataArray;
		public $enterpriseList;
		public $systemDomainListRequest;
		public $fileName;
		public $errorFileName;
		public $listOfAllSecurityDomain;
		public $addDeleteSecurityDomainArray;
		
		public function xmlHeader($sessionid, $commandType){
			$header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
			$header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
			$header .= "<command xsi:type=\"" . $commandType . "\" xmlns=\"\">";
			return $header;
		}
		
		public function xmlFooter(){
			return "</command>\n</BroadsoftDocument>\n";
		}

		public function buildCommandAuthenticationRequest($sessionid, $srcbwuserid){
			$xmlstr = $this->xmlHeader($sessionid, "AuthenticationRequest");
			$xmlstr .= "<userId>" . $srcbwuserid . "</userId>" . "\n";
			$xmlstr .= $this->xmlFooter();
			return $xmlstr;
		}
		
		public function checkTargetServerDetails(){ 
			$returnArray = array();
			try
			{
				$opts = array(
					'http' => array(
						'user_agent'=>'PHPSoapClient'
					),
					'ssl' => array(
						'verify_peer' => false,
						'verify_peer_name' => false
					)
				);

				$context = stream_context_create($opts);
				$soapClientOptions = array(
					'stream_context' => $context,
					'cache_wsdl' => WSDL_CACHE_NONE,
					"exceptions" => 0,
					"trace" => 1
				);
				libxml_disable_entity_loader(false);

				$soapClientOptions["exceptions"] = 1;
				try {
					$connectionUrl = $this->tgtbwprotocol."://" . $this->tgtbwip .":".$this->tgtbwport."/webservice/services/ProvisioningService?wsdl";
					$srcClient = new SoapClient($connectionUrl, $soapClientOptions);
					$charid = md5(uniqid(rand(), true));
					$srcsSessionid = substr($charid, 0, 32);
					$nonce = "";
					$response = $srcClient->processOCIMessage(array("in0" => $this->buildCommandAuthenticationRequest($srcsSessionid, $this->tgtbwuserid)));

					if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
					{
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						if ($xml->command["type"] != "Error")
						{
							$userName =  "ok";
							foreach ($xml->children() as $child)
							{
								$temp = $child->getName();
								if (!strcmp($temp, "sessionId"))
								{
									$srcsSessionid = $child;
								}
								foreach ($child->children() as $grandson)
								{
									$temp = $grandson->getName();
									if (!strcmp($temp, "nonce"))
									{
										$nonce = $grandson;
									}
									foreach ($grandson->children() as $greatson)
									{
										$temp = $greatson->getName();
										foreach ($greatson->children() as $ggson)
										{
											$temp = $ggson->getName();
										}
									}
								}
							}
						}
					}

					$S1 = sha1(trim($this->tgtbwpassword));
					$S2 = $nonce . ":" . $S1;
					$enc_pass = md5($S2);

					$xmlinput = $this->xmlHeader($srcsSessionid, "LoginRequest14sp4");
					$xmlinput .= "<userId>" . $this->tgtbwuserid . "</userId>";
					$xmlinput .= "<signedPassword>" . $enc_pass . "</signedPassword>";
					$xmlinput .= $this->xmlFooter();
					$response = $srcClient->processOCIMessage(array("in0" => $xmlinput));

					if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
					{
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						if ($xml->command["type"] != "Error")
						{
							$password =  "ok";
						}
					}
					else
					{
						$msg =  "No response at all from server. <br>";
					}

					if($userName == "ok" && $password == "ok")
					{
						$xmlstr = $this->xmlHeader($srcsSessionid, "SystemSoftwareVersionGetRequest");
						$xmlstr .= $this->xmlFooter();
						$response = $srcClient->processOCIMessage(array("in0" => $xmlstr));
						if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
						{
							$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
							if ($xml->command["type"] != "Error")
							{
								$returnArray["status"] = true;
								$returnArray["version"] = $xml->command->version;
								$returnArray["client"] = $srcClient;
								$returnArray["session"] = $srcsSessionid;
								return $returnArray;
							}
						}
					}
					else
					{
						$returnArray["status"] = false;
						return $returnArray;
					}
				}
				catch (SoapFault $E) {
				}
			}
			catch (Exception $e)
			{
				$code = $e->getCode();
				$str = $e->getMessage();
				exit(1);
			}
		}
		
		public function checkServerConnection(){
			$targetResponse = array();
			$targetResponse['Error'] = "";
			$targetServerConn = $this->checkTargetServerDetails();
			
			if($targetServerConn["status"]){
				$targetResponse['client'] = $targetServerConn["client"];
				$targetResponse['sessionid'] = $targetServerConn["session"];
			}else{
				$targetResponse['Error'] = "Target Server is not connected";
			}
			if(empty($targetResponse["Error"])){
				$this->targetclient = $targetResponse["client"];
				$this->tgtsessionid = $targetResponse["sessionid"];
			}else{
				echo "Target Server is not connected"; die;
			}
		}
		
		public function readCsvFile(){
			
			$csvData = array();
			$CSVfp = fopen($this->fileName, "r");
			if($CSVfp !== FALSE) {
				$c = 0;
				while(! feof($CSVfp)) {
					$data = fgetcsv($CSVfp, 1000, ",");
					$csvData[$c] = $data;
					$c++;
				}
			}
			fclose($CSVfp);
			$this->csvDataArray = $csvData;
		}
		
		public function assignSecurityDomainPattern(){
			$findSecurityDomainPattern = $this->csvDataArray[0][0];
			$findSecurityDomainPattern = explode(":", $findSecurityDomainPattern);
			$this->securityDomainPattern = trim($findSecurityDomainPattern[1]);//assign security domain pattern			
		}
		
		public function headerCSV(){
			$this->headerDataArray = $this->csvDataArray[1];
		}
		
		public function removeTwoRowsFromCSV(){
			array_shift($this->csvDataArray); //remove security domain pattern row from array
			array_shift($this->csvDataArray); //remove column name row from array
			array_pop($this->csvDataArray);//remove last blank element from array
		}
		
		public function validateCSVData(){
			$outputCSVRow = array();
			echo "Action,Enterprise/SP ID,All Groups, Group ID,Sec. Domain1,Sec. Domain2,Sec. Domain3,Sec. Domain4,Sec. Domain5 ";
			if(count($this->csvDataArray) > 0){
				foreach($this->csvDataArray as $key=>$val){
					$error = "";
					$validateEnterprise = $this->validateEnterprise($val[0]);
					if($validateEnterprise == ""){
						$validateGroup = $this->validateGroup($val);
						if($validateGroup == ""){
							$validateSecurityDomain = $this->validateSecurityDomain($val);						
							if(count($validateSecurityDomain) > 0){
								echo "\r\n".$error = $validateSecurityDomain;
							}else{ echo "";
								echo "\r\n".$this->makeProvisioningScript($val);
							}
						}else{
							echo $error = "Error : ".$validateGroup;
						}
					}else{
						echo $error = $validateEnterprise. " ". implode(",", $val);
					}
				}
			}
		}
		
		//validate enterprise data
		public function validateEnterprise($enterpriseId){		
			$enterpriseResponse = "";
			if(!in_array($enterpriseId, $this->enterpriseList)){
				$enterpriseResponse = "\r\nError : ".$enterpriseId." Enterprise Not Exist";
			}
			return $enterpriseResponse;
		}
		
		//validate group
		public function validateGroup($dataArray){
			$groupResponse = "";			
			if($dataArray[1] == "Yes"){ 
				if($dataArray[2] != ""){
					$groupResponse = "Group ID should be blank if All Groups is selected as Yes";
				}
			}else{
				//get group list for an enterprise
				$groupList = $this->getGroupList($dataArray[0]);
				if(!in_array($dataArray[2], $groupList)){
					return $groupResponse = "Group ID is not exist in this enterprise or broadworks";
				}
			}
			return $groupResponse;
		}
		
		//get group list for an enterprise
		public function getGroupList($enterpriseId){
			$allGroups = array();
			$xmlinput = $this->xmlHeader($this->tgtsessionid, "GroupGetListInServiceProviderRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($enterpriseId) . "</serviceProviderId>";
			$xmlinput .= $this->xmlFooter();
			$response = $this->targetclient->processOCIMessage(array("in0" => $xmlinput));
        	if(isset($response->processOCIMessageReturn) && $response->processOCIMessageReturn != "") {
        	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);	
        	    $a = 0;
        	    if (isset($xml->command->groupTable->row))
        	    {
        	        foreach ($xml->command->groupTable->row as $key => $value)
        	        {
        	            $allGroups[$a] = strval($value->col[0]);
        	            $a++;
        	        }
        	    }
        	}
			return $allGroups;
		}
		
		//validate security domain
		public function validateSecurityDomain($dataArray){
			$securityDomainStatus = array();
			$checkSecurityDomainColumnIsBlank = "true";
			for($i=3; $i < 8; $i++){
				if(!empty($dataArray[$i])){
					$this->listOfAllSecurityDomain[] = $dataArray[$i];
					if (!empty($this->securityDomainPattern) && strpos($dataArray[$i], $this->securityDomainPattern) === false ) {
						$securityDomainStatus[] = "( ".$dataArray[$i] ." do not match the security domain pattern )";
					}
					
					/*if (!in_array($dataArray[$i], $this->systemDomainListRequest)){
						$securityDomainStatus[] = "( ".$dataArray[$i] ." is not a valid domain for this enterprise )";
					}		*/
					$checkSecurityDomainColumnIsBlank = "false";
				}
			}
			if($checkSecurityDomainColumnIsBlank == "true"){
				$securityDomainStatus[] = "( There should be at least one security domain is required)";
			}
			$this->checkSecurityDomainColumnIsBlank = $checkSecurityDomainColumnIsBlank;
			if(count($securityDomainStatus) > 0){
				return "Error : ".implode(" && ", $securityDomainStatus);
			}else{
				return $securityDomainStatus;
			}
		}		
		
		public function readErrorXmlGenuine($xml) {
			global $supportEmailSupport;
			$errMsg = "";
			
			if (count ( $xml->command->attributes () ) > 0) {
				foreach ( $xml->command->attributes () as $a => $b ) {
					if ($b == "Error" || $b == "Warning") {
						if ($xml->command->summaryEnglish) {
							$errMsg .= "%0D%0A" . strval ( $xml->command->summary );
						}
					}
				}
			} else {
				$errMsg .= "Success";
			}
			return $errMsg;
		}
		
		public function getEnterpriseList(){
			$ociResponse["Error"] = "";
			$ociResponse["Success"] = "";

			$xmlinput = $this->xmlHeader($this->tgtsessionid, "ServiceProviderGetListRequest");
			$xmlinput .= $this->xmlFooter();
			
			$response = $this->targetclient->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if ($this->readErrorXmlGenuine($xml) != "") {
				$ociResponse["Error"] = strval($xml->command->summaryEnglish);
			}else{
				if(count($xml->command->serviceProviderTable->row) > 0){
					$a = 0;
					foreach ($xml->command->serviceProviderTable->row as $k => $v){
						$spList[$a] = strval($v->col[0]);
						$a++;
					}
				}
				$this->enterpriseList = $spList;
			}
			
			return $ociResponse;
		}
		
		public function getSystemDomainListRequest(){
			$ociResponse = "";
			$domainList = array();
			$xmlinput = $this->xmlHeader($this->tgtsessionid, "SystemDomainGetListRequest");
			$xmlinput .= $this->xmlFooter();			
			$response = $this->targetclient->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if ($this->readErrorXmlGenuine($xml) != "") {
				if(!empty($xml->command->detail)){
					echo $ociResponse = strval($xml->command->detail); die;
				}else{
					echo $ociResponse = strval($xml->command->summaryEnglish); die;
				}
			}else{				
				foreach($xml->command->domain as $key=>$val){
					if (!empty($this->securityDomainPattern) && strpos($val, $this->securityDomainPattern) !== false ) {
						$domainList[] = strval($val);
					}
				}
				$this->systemDomainListRequest = $domainList;
				$ociResponse = "Success";
			}			
		}
		
		public function makeProvisioningScript($dataArray){
			return "Add,".implode(",", $dataArray);
		}
		
		public function makeListOfDeleteArray(){
			$listOfAllDomainPresentInCSV = array_unique($this->listOfAllSecurityDomain);
			//echo "\r\nCSV Domain List :- ";print_r($listOfAllDomainPresentInCSV);
			//echo "\r\nSystem List :- ";print_r($this->systemDomainListRequest);
			$e = 0;
			$enterpriseListArray = array();
			$csvFinalArrayToWrite = array();
			foreach($this->systemDomainListRequest as $key=>$val){
				if (!in_array($val, $listOfAllDomainPresentInCSV)){
					$enterpriseList = $this->getEnterpriseListOfSecurityDomain($val);
					if(empty($enterpriseList["Error"])){
						$enterpriseListArray[$e]["securityDomain"] = $val;
						$enterpriseListArray[$e]["enterpriseId"] = $enterpriseList["Success"];
						$g = 0;
						foreach($enterpriseList["Success"] as $key1=>$val1){
							$groupList = $this->getGroupListOfSecurityDomain($val1, $val);
							if(empty($groupList["Error"])){
								//$enterpriseListArray[$e]["groupList"][$g] = $groupList;
								//$csvFinalArrayToWrite[$val1] = $groupList["Success"];
								foreach($groupList["Success"] as $key2=>$val2){
									$csvFinalArrayToWrite[$val1]["groupList"][$val2][] = $val;
									$csvFinalArrayToWrite[$val1]["securityDomain"][$val][] = $val2;									
																		
								}
							}
							$g++;
							$csvFinalArrayToWrite[$val1]["totalNumberOfGroupsInEnterprise"] = $this->getGroupsOfAnENterprise($val1);
							$csvFinalArrayToWrite[$val1]["totalNumberOfGroupsBsedOnSecurityDomain"] = count($groupList["Success"]);
						}
						$e++;
					}					
				}
			}
			$this->addDeleteSecurityDomainArray = $csvFinalArrayToWrite;
			//print_r($enterpriseListArray);print_r($csvFinalArrayToWrite);die;
		}		
		
		public function getEnterpriseListOfSecurityDomain($securityDomain){
			$ociResponse = array();

			$xmlinput = $this->xmlHeader($this->tgtsessionid, "SystemDomainGetAssignedServiceProviderListRequest");
			$xmlinput .= "<domain>".htmlspecialchars($securityDomain)."</domain>";
			$xmlinput .= $this->xmlFooter();
			
			$response = $this->targetclient->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if ($this->readErrorXmlGenuine($xml) != "") {
				$ociResponse["Error"] = strval($xml->command->summaryEnglish);
			}else{
				if(count($xml->command->serviceProviderTable->row) > 0){
					$a = 0;
					foreach ($xml->command->serviceProviderTable->row as $k => $v){
						$spList[$a] = strval($v->col[0]);
						$a++;
					}
				}
				$ociResponse["Success"] = $spList;
			}
			
			return $ociResponse;
		}
		
		public function getGroupListOfSecurityDomain($enterpriseId, $securityDomain){
			$ociResponse = array();

			$xmlinput = $this->xmlHeader($this->tgtsessionid, "ServiceProviderDomainGetAssignedGroupListRequest");
			$xmlinput .= "<serviceProviderId>".htmlspecialchars($enterpriseId)."</serviceProviderId>";
			$xmlinput .= "<domain>".htmlspecialchars($securityDomain)."</domain>";
			$xmlinput .= $this->xmlFooter();
			
			$response = $this->targetclient->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if ($this->readErrorXmlGenuine($xml) != "") {
				$ociResponse["Error"] = strval($xml->command->summaryEnglish);
			}else{
				if(count($xml->command->groupTable->row) > 0){
					$a = 0;
					foreach ($xml->command->groupTable->row as $k => $v){
						$groupList[$a] = strval($v->col[0]);
						$a++;
					}
				}
				$ociResponse["Success"] = $groupList;
			}
			
			return $ociResponse;
		}
		
		public function showListOfDeleteArray(){
			//print_r($this->addDeleteSecurityDomainArray);
			//echo "\r\n";
			$rowCsv = array();
			
			$a = 0;
			foreach($this->addDeleteSecurityDomainArray as $key=>$val){				
					foreach($val["groupList"] as $key1=>$val1){
						//if($val["allGroups"] == )
							$rowCsv[$a]["action"] = "Delete";
							$rowCsv[$a]["enterpriseId"] = $key;
							
							for($e = 0; $e < 6; $e++){
								if(!empty($val1[$e])){
									$rowCsv[$a]["securityDomain"][$e] = $val1[$e];
								}else{
									$rowCsv[$a]["securityDomain"][$e] = "";
								}
							}
							if($val["totalNumberOfGroupsInEnterprise"] == $val["totalNumberOfGroupsBsedOnSecurityDomain"]){
								$rowCsv[$a]["allGroups"] = "Yes";
								$rowCsv[$a]["groupId"] = "";
								continue;
							}else{
								$rowCsv[$a]["allGroups"] = "No";
								$rowCsv[$a]["groupId"] = $key1;
							}
							$a++;
					}
				
			}
			foreach($rowCsv as $key=>$val){
					echo "\r\n".$val['action'].",".$val['enterpriseId'];
					echo ",".$val["allGroups"];
					echo ",".$val['groupId'];
					echo ",".implode(",", $val['securityDomain']);
			}
			echo "\r\n";
		}	
		
		//get the total number of groups in an enterprise
		public function getGroupsOfAnENterprise($enterpriseId){
			$ociResponse = array();
			$allGroups = array();
			
			$xmlinput = $this->xmlHeader($this->tgtsessionid, "GroupGetListInServiceProviderRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($enterpriseId) . "</serviceProviderId>";
			$xmlinput .= $this->xmlFooter();			
			$response = $this->targetclient->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if ($this->readErrorXmlGenuine($xml) != "") {
				if(!empty($xml->command->detail)){
					echo $ociResponse = strval($xml->command->detail); die;
				}else{
					echo $ociResponse = strval($xml->command->summaryEnglish); die;
				}
			}else{				
				$a = 0;
        	    if (isset($xml->command->groupTable->row))
        	    {
        	        foreach ($xml->command->groupTable->row as $key => $value)
        	        {
        	            $allGroups[$a] = strval($value->col[0]);
        	            $a++;
        	        }
        	    }
				$ociResponse = $allGroups;
			}	
			return count($ociResponse);
		}
	}
	
	

	
	$expressScriptObj = new expressScript();
	//Broadworks Login Details                
	$expressScriptObj->tgtbwip 				= "10.100.10.211"; //ip address of broadworks
	$expressScriptObj->tgtbwprotocol      	= "http"; //protocol
	$expressScriptObj->tgtbwuserid          = "jeeteshAdmin"; //express user name
	$expressScriptObj->tgtbwpassword     	= "s00a1_na!"; //express password
	$expressScriptObj->tgtbwport            = "80"; //port
	$expressScriptObj->ociVersion           = "22"; //oci version
	$expressScriptObj->fileName           	= $argv[1];; //oci version
	
	$expressScriptObj->checkServerConnection();	//check connection
	$expressScriptObj->readCsvFile(); //read csv file
	$expressScriptObj->assignSecurityDomainPattern(); //get list of security domain
	$expressScriptObj->headerCSV(); //get the header column of csv
	$expressScriptObj->getSystemDomainListRequest(); //get system domain list of bw
	$expressScriptObj->getEnterpriseList(); //get enterprise list of bw
	$expressScriptObj->removeTwoRowsFromCSV(); //get final security domain data after removing two lines for pattern and column name
	$expressScriptObj->validateCSVData(); //get final security domain data after removing two lines for pattern and column name
	$expressScriptObj->makeListOfDeleteArray(); //get delete list security domain data of enterprise, group id
	$expressScriptObj->showListOfDeleteArray(); //get delete list security domain data of enterprise, group id
	

?>